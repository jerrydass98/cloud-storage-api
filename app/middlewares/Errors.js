/**
 * @author Jerome Dass
 */

module.exports = class myErrors extends Error {

    constructor(message) {
        super(message.message);
        this.message = message;
        this.name = 'Error';
    }

    static NotFoundError(message) {
        message = {
            status: 404,
            header: 'Resource_Not_Found',
            message: message,
        };
        const error = new myErrors(message);
        return error;
    }

    static BadRequest(message) {
        message = {
            status: 400,
            header: 'Bad_Request',
            message: message,
        };
        const error = new myErrors(message);
        return error;
    }

    static UnauthorizedRequest(message) {
        message = {
            status: 401,
            header: 'Unauthorized_request',
            message: message,
        };
        const error = new myErrors(message);
        return error;
    }

    static ForbiddenRequest(header = 'Forbidden_Request', message) {
        message = {
            'status': 403,
            'header': header,
            'message': message,
        };
        const error = new myErrors(message);
        return error;
    }
}