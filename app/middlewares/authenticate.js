/**
 * @author Jerome Dass
 */

'use strict';

const jwt = require('jsonwebtoken');
const queryProcess = require('./query-process');
const Errors = require('./../middlewares/Errors');
const modifyResponse = require('./modify-responses');

async function generateAccessToken(user,save = true) {
    try{
        const date = new Date();
        date.setMinutes(date.getMinutes() + 4);
        const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '300s' });
        const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '86400s' });
        const obj = {
            'access_token': token,
        }
        if (save) {
            obj['token_expires_at'] = date.getTime();
            obj['refresh_token'] = refreshToken;
        }
        return obj;
    } catch(err) {
        throw Errors.BadRequest(err.message);
    }
}

function authenticateContext(req,res,next) {
    try{
        if (req.context.user) {
            queryProcess(req, res, next);
        } else if (req.context.valid_till) {
            let date = new Date(req.context.valid_till);
            if (
                req.originalUrl !== '/auth/login' &&
                req.originalUrl !== '/auth/forgot-password' &&
                req.originalUrl !== '/auth/reset-password' &&
                req.originalUrl !== '/auth/register' ||
                date < new Date()
            ) {
                throw Errors.UnauthorizedRequest('Not authorized');
            } else {
                queryProcess(req, res, next);
            }
        }
    } catch(err) {
        modifyResponse(err,req,res);
    }
}

async function authenticateToken(req,res,next) {
    try{
        const authHeader = req.headers['authorization'];
        let token = authHeader && authHeader.split(' ')[1];
        if (req.originalUrl.includes('/stream')) {
            token = req.cookies.authorization;
        }
        if (req.originalUrl === '/token' && req.method === 'POST') {
            const obj = {};
            if (!token) {
                let date = new Date();
                date.setMinutes(date.getMinutes() + 15);
                obj['valid_till'] = date;
                obj['purpose'] = 'requesting access';
                res.status(200).send(await generateAccessToken(obj,false));
            } else {
                try{
                    token = await jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
                } catch (err) {
                    if (err.name === 'TokenExpiredError') {
                        token = null;
                    }
                }
                if (!token || token.valid_till && token.iat) {
                    let date = new Date();
                    date.setMinutes(date.getMinutes() + 15);
                    obj['valid_till'] = date;
                    res.status(200).send(await generateAccessToken(obj,false));
                } else {
                    req.headers['authorization'] = token;
                    res.redirect('/auth/user/me');
                }
            }

        } else if (req.originalUrl === '/refresh-token' && req.method === 'POST') {
            try{
                token = await jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
                const obj = await generateAccessToken({ 'user': token.user});
                delete obj.refresh_token;
                //res.cookie('authorization', obj.access_token, { domain: '192.168.43.12', path: '/' });
                res.status(200).send(obj);
            } catch (err) {
                if (err.name === 'TokenExpiredError') {
                    throw Errors.UnauthorizedRequest('Session expired');
                }
            }
        }else if (!token) {
            throw Errors.ForbiddenRequest('Invalid authorization or session');
        } else if (req.originalUrl === '/logout' && req.method === 'DELETE') {
            res.status(204);
            res.end();
        } else {
            jwt.verify(token,process.env.ACCESS_TOKEN_SECRET, (err, context) => {
                if (err) {
                    throw Errors.ForbiddenRequest(err.name, err.message);
                } else if (
                    context?.purpose &&
                    context?.valid_till &&
                    ![
                        '/auth/login',
                        '/auth/register',
                        '/auth/forgot-password',
                        '/auth/reset-password',
                    ].includes(req.originalUrl)
                ) {
                    throw Errors.ForbiddenRequest('Invalid token');
                } else {
                    req['context'] = { ...context };
                    authenticateContext(req, res, next);
                }
            });
        }
    } catch(err) {
        modifyResponse(err,req,res);
    };
}

module.exports = {
    generateAccessToken,
    authenticateContext,
    authenticateToken,
}