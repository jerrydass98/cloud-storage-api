/**
 * @author Jerome Dass
 */

'use strict';

module.exports = function (err, req, res, next) {
    if (err.message?.status) {
        res.status(err.message.status);
        res.write(JSON.stringify({err_code:err.message.header,message:err.message.message}));
    } else {
        res.status(500);
        console.log(err);
        res.write(JSON.stringify({ err_code: 'Internal_Server_Error', message: 'internal server error'}));
    }
    res.end();
}