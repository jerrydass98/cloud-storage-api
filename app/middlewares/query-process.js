/**
 * @author Jerome Dass
 */

'use strict';

function queryProcess(req, res, next) {
    req.criteria = {
        filter: {
            'path': null,
            'name': null,
        },
        fields: '',
        options: {
            limit: 3,
            skip: 0,
        }
    }
    if (req.query.filter) {
        req.criteria.filter = {...JSON.parse(req.query.filter)};
    }
    if (req.query.limit) {
        req.criteria.options.limit = req.query.limit;
    }
    if (req.query.skip) {
        req.criteria.options.skip = req.query.skip;
    }
    if (req.query.fields) {
        req.criteria.fields = req.query.fields;
    }
    next();
}

module.exports = queryProcess;