/**
 * @author Jerome Dass
 */

'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const otpSchema = Schema({
    user_id: { type: mongoose.Types.ObjectId, ref: 'users'},
    email: { type: String, required: true },
    code: { type: String, required: true },
    expires_at: { type: Date, required: true },
    status: {
        type: String,
        required: true,
        enum: ['waiting','verified'],
        default: 'waiting',
    }
},{
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('otp_verifications',otpSchema);