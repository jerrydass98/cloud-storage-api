/**
 * @author Jerome Dass
 */

'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = Schema({
    name: String,
    email: String,
    user_name: String,
    phone_no: Number,
    password: String,
    is_verified_email: { type: Boolean, default: false },
    is_verified_phone: { type: Boolean, default: false },
    is_deleted: { type: Boolean, default: false },
    deleted_at: { type: Date, default: null },
    password_token: String,
    password_token_expiry: Date,
    status: {
        type: String,
        required: true,
        enum: ['active','suspended','deleted'],
    }
},{
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
}
);

module.exports = mongoose.model('users',UserSchema);