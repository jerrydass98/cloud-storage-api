/**
 * @author Jerome Dass
 */

'use strict';
const Users = require('../models/users');
const OTP = require('./../models/otp');
const Q = require('q');
const bcrypt = require('bcrypt');
const nodemailer = require("nodemailer");
const { generateAccessToken }  = require('./../middlewares/authenticate');
const DataController = require('./data');
const Errors = require('./../middlewares/Errors');

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: '',
        pass: '',
    }
});

module.exports = class UsersController {

    static login(context, document, token) {
        return Q.try(() => Users.findOne({'$or':[
                { 'email': document.email },
                { 'user_name': document.email },
            ],
            'deleted_at': null,
            'is_deleted':false,
        }).lean())
            .then(user => {
                if (!user) {
                    throw Errors.NotFoundError('User not found');
                }
                return Q.all([
                    user,
                    bcrypt.compare(document.password,user.password),
                ]);
            })
            .then(([user, results]) => {
                if (!results) {
                    throw Errors.BadRequest('Password incorrect');
                }
                delete user.password;
                const temp = {
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                };
                return Q.all([
                    generateAccessToken({ 'user': temp }),
                    user,
                ]);
            })
            .then(([userToken,user]) => {
                return {
                    ...user,
                    ...userToken,
                };
            });
    }

    static create(context, document, token) {
        return Q.all([
            bcrypt.hash(document.password,12),
            Users.findOne({ 'email': document.email },'_id').lean(),
            Users.findOne({ 'user_name': document.user_name },'_id').lean(),
            Users.findOne({ 'phone_no': document.phone_no, is_verified_phone: true },'_id').lean(),
        ])
            .then(([hashedPwd, duplicateEmail, duplicateUName, duplicatePNo]) => {
                if (duplicateUName) {
                    throw Errors.BadRequest('username already taken.');
                } else if (duplicateEmail) {
                    throw Errors.BadRequest('email already exists.');
                } else if (duplicatePNo) {
                    throw Errors.BadRequest('phone number already taken.');
                }
                let user = new Users({
                    name: document.name,
                    email: document.email,
                    phone_no: document.phone_no,
                    user_name: document.user_name,
                    is_delete: false,
                    status: 'active',
                    password: hashedPwd,
                });
                return user.save();
            })
            .then(result=>{
                if(!result) {
                    throw Errors.BadRequest('Failed to create user.');
                }
                result = result.toJSON();
                delete result.password;
                const user = {
                    _id: result._id.toString(),
                    name: result.name,
                    email: result.email,
                };
                return Q.all([
                    generateAccessToken({ user: user }),
                    result,
                    DataController.createDirectory({ user: user },{
                        name: '',
                        path: '',
                    }),
                ]);
            })
            .then(([token, result]) => {
                return {
                    ...result,
                    ...token,
                };
            });
    }

    static requestOTP(context) {
        return Q.try(() => {
            return OTP.findOne(
                {
                    user_id: context.user._id,
                    status: 'waiting',
                    expires_at: { $gt: new Date()},
                }).lean();
        })
            .then(async result => {
                if (result) {
                    throw Errors.BadRequest('Cannot request until active otp expires.');
                }
                const digits = '0123456789';
                let otp = '';
                for (let i = 0; i < 6; i++ ) {
                    otp += digits[Math.floor(Math.random() * 10)];
                }
                const date = new Date();
                date.setMinutes(date.getMinutes() + 5);
                const document = {
                    email: context.user.email,
                    user_id: context.user._id,
                    code: otp,
                    expires_at: date,
                }
                return Q.all([
                    OTP.create(document),
                    transporter.sendMail(
                        {
                            from: '"Jerry" <jeromedass98@gmail.com>',
                            to: `${context.user.email}`,
                            subject: 'OTP requested',
                            text: 'OTP: '+otp,
                        }
                    ),
                ]);
            });
    }

    static verifyOTP(context, document) {
        if (!document.code) {
            throw Errors.BadRequest('No OTP provided');
        }
        if (
            !document.purpose ||
            ![
                'verify_email',
                'forgot_password'
            ].includes(document.purpose)
        ) {
            throw Errors.BadRequest('No OTP provided');
        }
        return Q.all([
            OTP.findOne({
                code: document.code,
                status: 'waiting',
                user_id: context.user._id,
                email: context.user.email,
                expires_at: { $gt: new Date() }
            }).lean(),
        ])
            .then(async ([otp]) => {
                if (!otp) {
                    throw Errors.BadRequest('No active otp.');
                }
                const promises = [
                    OTP.updateOne(
                        { _id: otp._id.toString() },
                        { '$set': { 'status': 'verified' }},
                    ).lean()
                ];
                if (document.purpose === 'verify_email') {
                    promises.push(Users.findOneAndUpdate(
                        { _id: context.user._id },
                        { $set: { 'is_verified_email': true }},
                    ).lean());
                } else if (document.purpose === 'forgot_password') {
                    const text = Math.random().toString(36).slice(2, 10);
                    document.password = await bcrypt.hash(text,12);
                    promises.push(Users.updateOne(
                        { _id: context.user._id },
                        { $set: { 'password': document.password }},
                    ).lean());
                    promises.push(transporter.sendMail(
                        {
                            from: '"Jerry" <jeromedass98@gmail.com>',
                            to: context.user.email,
                            subject: 'Password changed',
                            text: `Hi ${context.user.name}, your password has been changed to ${text}.`,
                        }
                    ))
                }
                return Q.all(promises);
            })
            .then(([otp,user]) => {
                return user;
            })
    }

    static findById(context, id) {
        return Q.try(() => Users.findOne({ _id: id },{ password: 0 }).lean())
    }

    static editAccount(context, document) {
        delete document.password;
        delete document.status;
        delete document.is_verified_email;
        delete document.is_verified_phone;
        delete document.is_deleted_at;
        return Q.try(() => Users.findOneAndUpdate(
            { _id: context.user._id },
            { '$set': document },
            { new: true }).lean(),
        )
            .then(user => {
                delete user.password;
                const temp = {
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                };
                return Q.all([
                    generateAccessToken({ 'user': temp }),
                    user,
                ]);
            })
            .then(([userToken,user]) => {
                return {
                    ...user,
                    ...userToken,
                };
            });
    }

    static forgotPassword(document) {
        if (!document.email) {
            throw Errors.BadRequest('email not specified.');
        }
        return Q.try(() => Users.findOne({'email':document.email}).lean())
            .then(user => {
                if (!user) {
                    throw Errors.NotFoundError('User not found');
                }
                const password_token = require('crypto').randomBytes(20).toString('hex');
                const date = new Date();
                date.setMinutes(date.getMinutes() + 60);
                return Q.all([
                    Users.updateOne(
                        {_id: user._id},
                        {'$set': { password_token,password_token_expiry: date }},
                    ),
                    transporter.sendMail(
                        {
                            from: '"Jerry" <jeromedass98@gmail.com>',
                            to: user.email,
                            subject: 'Password reset link',
                            text: `Click on the link to reset Password http://localhost:3001/reset-password?password_token=${password_token}&userid=${user._id.toString()}`,
                        }
                    ),
                ]);
            });
    }

    static resetPassword(document) {
        return Q.all([
            bcrypt.hash(document.new_password,12),
            Users.findOne(
                {
                    _id: document.userid,
                    password_token: document.password_token,
                    password_token_expiry: { $gte: new Date() },
                }).lean(),
        ])
            .then(([hashedPwd,user]) => {
                if (!user) {
                    throw Errors.NotFoundError('User not found');
                }
                return Users.updateOne(
                    {_id: user._id.toString()},
                    { 
                        '$set': {password: hashedPwd},
                        '$unset': {password_token:'',password_token_expiry:''}
                    },{new:true}
                );
            });
    }
}