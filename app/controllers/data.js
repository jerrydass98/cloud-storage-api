/**
 * @author Jerome Dass
 */

'use strict';

const fs = require('fs');
const fileType = require('file-type');
const Path = require("path")
const Q = require('q');
const Errors = require('./../middlewares/Errors');
const { resolve } = require('path');

const PATH = process.env.HOME_PATH;

module.exports = class DataController {

   static getFile(context, criteria) {
      let path = Path.join(PATH, context.user._id);
      return Q.try(() => {
         if (criteria.filter.path) {
            path = Path.join(path, criteria.filter.path);
         }
         if (criteria.filter.name) {
            path = Path.join(path, decodeURIComponent(criteria.filter.name));
         }
         return path;
      });
   }

   static getDirectory(context, criteria) {
      let path = '';
      return Q.try(() => {
         return new Promise((resolve, reject) => {
            path = Path.join(PATH, context.user._id);
            if (criteria.filter.path && criteria.filter.path !== '') {
               path = Path.join(path,criteria.filter.path);
            }
            if (criteria.filter.name && criteria.filter.name !== '') {
               path = Path.join(path,criteria.filter.name);
            }
            fs.readdir(path, { withFileTypes: true }, (err, data) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(data);
               }
            });
         });
      })
         .then(async (results) => {
            const len = results.length;
            const content = {
               'folders': [],
               'files': [],
            };
            for (let i=0; i<len; i++) {
               if (results[i].isFile()) {
                  const stream = fs.createReadStream(Path.join(path,results[i]['name']));
                  results[i].mime = await fileType.fromStream(stream) || { ext: results[i].name.split('.').pop(), mime: results[i].name.split('.').pop() };
                  content['files'].push(results[i]);
               } else {
                  content['folders'].push(results[i]);
               }
            }
            return content;
         });
   }

   static editData(context, document) {
      return Q.try(() => {
         return new Promise((resolve, reject) => {
            let path = Path.join(PATH, context.user._id);
            if (document.path && document.path !== '') {
               path = Path.join(path,document.path);
            }
            let newPath = path;
            if (document.name && document.name !== '') {
               path = Path.join(path,document.name);
            }
            if (document.name && document.newName !== '') {
               newPath = Path.join(newPath,document.newName);
            }
            fs.rename(path, newPath, (err) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(null);
               }
            });
         });
      })
         .then(() => {
            return true;
         });
   }

   static createData(context, document, files) {
      if (document.type === 'file') {
         return DataController.createFile(context, document, files || {});
      } else if(document.type === 'folder') {
         return DataController.createDirectory(context, document);
      } else {
         return false;
      }
   }

   static createFile(context,document, files) {
      if (document.path.includes('..')) {
         throw Errors.BadRequest('range unavailable');
      }
      return Q.try(() => files.file.mv(Path.join(PATH, context.user._id, document.path, files.file.name)) )
         .then(() => {
            return true;
         })
   }

   static createDirectory(context, document) {
      return Q.try(() => {
         return new Promise((resolve, reject) => {
            let path = Path.join(PATH, context.user._id);
            if (document.path && document.path !== '') {
               path = Path.join(path,document.path);
            }
            if (document.name && document.name !== '') {
               path = Path.join(path,document.name);
            }
            fs.mkdir(path,{ withFileTypes: true }, (err) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(true);
               }
            });
         });
      });
   }

   static deleteData(context, criteria) {
      if (criteria.filter.type === 'file') {
         return DataController.deleteFile(context, criteria);
      } else if(criteria.filter.type === 'folder') {
         return DataController.deleteDirectory(context, criteria);
      }
   }

   static deleteFile(context, criteria) {
      return Q.try(() => {
         return new Promise((resolve, reject) => {
            let path = Path.join(PATH, context.user._id);
            if (criteria.filter.path && criteria.filter.path !== '') {
               path = Path.join(path,criteria.filter.path);
            }
            if (criteria.filter.name && criteria.filter.name !== '') {
               path = Path.join(path,criteria.filter.name);
            }
            fs.unlink(path, (err) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(null);
               }
            });
         });
      })
         .then(() => {
            return true;
         });
   }
   
   static deleteDirectory(context, criteria) {
      return Q.try(() => {
         return new Promise((resolve, reject) => {
            let path = Path.join(PATH, context.user._id);
            if (criteria.filter.path && criteria.filter.path !== '') {
               path = Path.join(path,criteria.filter.path);
            }
            if (criteria.filter.name && criteria.filter.name !== '') {
               path = Path.join(path,criteria.filter.name);
            }
            fs.rmdir(path, {recursive: true, force: true}, (err) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(true);
               }
            });
         });
      });
   }

   static copyData(context, document) {
      return Q.try(() => {
         document.oldPath = Path.join(PATH,context.user._id,document.oldPath,document.file);
         document.newPath = Path.join(PATH,context.user._id,document.newPath,document.file);
         return new Promise((resolve,reject) => {
            fs.copyFile(document.oldPath,document.newPath,(err) => {
               if (err) {
                  reject(err);
               }
               else { resolve(true);}
            });
         });
      })
   }

   static moveData(context, document) {
      return Q.try(() => {
         document.oldPath = Path.join(PATH,context.user._id,document.oldPath,document.file);
         document.newPath = Path.join(PATH,context.user._id,document.newPath,document.file);
         return new Promise((resolve,reject) => {
            fs.rename(document.oldPath,document.newPath,(err) => {
               if (err) {
                  reject(err);
               }
               else { resolve(true);}
            });
         });
      })
   }
};