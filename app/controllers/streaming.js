/**
 * @author Jerome Dass
 */

'use strict';

const fs = require('fs');
const Path = require('path');
const Errors = require('./../middlewares/Errors');

const PATH = process.env.HOME_PATH;

module.exports = class StreamController {

   static stream(context, criteria, range, res) {
      const fileName = Path.join(PATH, context.user._id, criteria.filter.path, decodeURIComponent(criteria.filter.name));
      if (!range) {
         throw Errors.BadRequest('range unavailable');
      }
      const mediaSize = fs.statSync(fileName).size;
      const start = Number(range.replace(/\D/g, ''));
      const chunkSize = 10**6;
      const end = Math.min(start+chunkSize,mediaSize-1);
      const contentLength = end - start + 1;
      const headers = {
         'Content-Range': `bytes ${start}-${end}/${mediaSize}`,
         'Accept-Range': 'bytes',
         'Content-Length': contentLength,
         'Content-Type': 'video/mp4',
      }
      res.writeHead(206,headers);
      const mediaStream = fs.createReadStream(fileName, { start, end } );
      mediaStream.pipe(res);
   }
};