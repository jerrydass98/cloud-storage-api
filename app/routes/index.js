'use strict';

module.exports = [
    require('./auth'),
    require('./data'),
    require('./stream'),
];