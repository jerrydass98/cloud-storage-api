/**
 * @author Jerome Dass
 */

'use strict';
const router = require('express').Router();
const UserController = require('./../controllers/users');

router.post('/auth/login', (req,res,next) => {
    const token = req.headers.authorization.split(' ')[1];
    UserController.login(req.context,req.body, token)
        .then(result => res.send({ 'success': true, data: result}))
        .catch(next);
});

router.post('/auth/register', (req,res,next) => {
    const token = req.headers.authorization.split(' ')[1];
    UserController.create(req.context,req.body, token)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.put('/auth/user/me', (req, res, next) => {
    UserController.editAccount(req.context,req.body)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.get('/auth/forgot-password', (req, res, next) => {
    UserController.resetPassword(req.context)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.get('/auth/user/me', (req, res, next) => {
    UserController.findById(req.context,req.context.user._id)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.post('/auth/otp/request', (req, res, next) => {
    UserController.requestOTP(req.context, req.body)
        .then(result => res.send({ 'success': true }))
        .catch(next);
});

router.post('/auth/otp/verify', (req, res, next) => {
    UserController.verifyOTP(req.context, req.body)
        .then(result => res.send({ 'success': true }))
        .catch(next);
});

router.post('/auth/forgot-password', (req, res, next) => {
    UserController.forgotPassword(req.body)
        .then(result => res.send({ 'success': true }))
        .catch(next);
});

router.post('/auth/reset-password', (req, res, next) => {
    UserController.resetPassword(req.body)
        .then(result => res.send({ 'success': true }))
        .catch(next);
});

module.exports = router;