/**
 * @author Jerome Dass
 */

'use strict';
const router = require('express').Router();
const SteamController = require('../controllers/streaming');

router.get('/stream', (req, res, next) => {
    try{
        SteamController.stream(req.context, req.criteria, req.headers.range, res);
    } catch (err) {
        console.log(err);
        next;
    }
});

module.exports = router;