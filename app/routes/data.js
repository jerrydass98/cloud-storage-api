/**
 * @author Jerome Dass
 */

'use strict';
const router = require('express').Router();
const DataController = require('../controllers/data');

router.get('/data', (req, res, next) => {
    DataController.getDirectory(req.context, req.criteria)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.get('/data/file', (req, res, next) => {
    DataController.getFile(req.context, req.criteria)
        .then(result => res.sendFile(result))
        .catch(next);
});

router.put('/data', (req, res, next) => {
    DataController.editData(req.context, req.body)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.post('/data', (req, res, next) => {
    DataController.createData(req.context, req.body, req.files)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.delete('/data', (req, res, next) => {
    DataController.deleteData(req.context, req.criteria)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.put('/data/copy', (req, res, next) => {
    DataController.copyData(req.context, req.body)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

router.put('/data/move', (req, res, next) => {
    DataController.moveData(req.context, req.body)
        .then(result => res.send({ 'success': true, data: result }))
        .catch(next);
});

module.exports = router;