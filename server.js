/**
 * @author Jerome Dass
 */

'use strict';

require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const routes = require('./app/routes/index');
const app = express();
const authencticate = require('./app/middlewares/authenticate');
const modifyResponse = require('./app/middlewares/modify-responses');
const cookieParser = require('cookie-parser');
//const { createClient } = require('redis');

// const client = createClient({
//     url: 'redis://localhost:6379',
// });

// client.connect();

// client.on('connect',() => {
//     console.log('Redis connection successful');
// });

// client.on('error',(err) => {
//     console.log('Error:',err);
// });

//process.redis = client;

mongoose.connect(process.env.MONGODB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
    console.log('connection successful');
});

mongoose.connection.on('error', (err) => {
    console.log('Error while connecting to db\n',err);
})

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(fileUpload());
app.use(cors());
app.use(authencticate.authenticateToken);
app.use('/',routes);
app.use(modifyResponse);

const server = app.listen(process.env.PORT,()=>{
    console.log('Server started @',process.env.PORT);
});

process.on("SIGTERM",()=>{
    console.info("SIGTERM signal received");
    server.close(()=>{
        console.log("Graceful shutdown");
        mongoose.connection.close(false,()=>{
            console.log("Database connection closed.");
            process.exit(0);
        });
    });
});